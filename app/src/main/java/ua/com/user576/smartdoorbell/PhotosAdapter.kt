package ua.com.user576.smartdoorbell

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.storage.FirebaseStorage
// import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class PhotosAdapter(private val activityContext : Context, options : FirebaseRecyclerOptions<DoorbellRecord>) :
    FirebaseRecyclerAdapter<DoorbellRecord, PhotosAdapter.DoorbellRecordViewHolder>(options) {

    class DoorbellRecordViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val tvRingTime : TextView = view.findViewById(R.id.ring_time_text_view)
        val ivPhoto : ImageView = view.findViewById(R.id.photo)
        var key : Int = 0

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            if (view == null) {
                Log.w(LOG_TAG, VIEW_IS_NULL)
            } else {
                Log.w(LOG_TAG, this.key.toString())
                val activity = view.context
                if (activity is MainActivity) {
                    activity.toResolveRequestFragment(
                        bundleOf(
                            ResolveRequestFragment.DOORBELL_RECORD_KEY to this.key,
                            ResolveRequestFragment.TIME_INSTANT_READABLE_STRING_KEY to this.tvRingTime.text.toString()
                        )
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoorbellRecordViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        val colorRes = when(viewType) {
            0 -> android.R.color.holo_green_light
            1 -> R.color.second_request
            else -> android.R.color.holo_red_light
        }
        view.setBackgroundColor(
            ContextCompat.getColor(activityContext, colorRes)
        )
        return DoorbellRecordViewHolder(view)
    }

    private val formatter = SimpleDateFormat(activityContext.getString(R.string.format_pattern), Locale.getDefault())

    override fun onBindViewHolder(holder: DoorbellRecordViewHolder, position: Int, record: DoorbellRecord) {
        holder.key = record.key
        holder.tvRingTime.text = activityContext.getString(
            R.string.rang_at, formatter.format(Date(record.timeInstant * 1000))
        )
        val ref = FirebaseStorage.getInstance().getReferenceFromUrl(record.imageUrl)
        //Picasso.get().load(ref).into(holder.ivPhoto)
        Glide.with(activityContext)
            .load(ref)
            .into(holder.ivPhoto)
    }

    override fun getItemViewType(position: Int): Int = position % DIFFERENT_COLORS_NUMBER

    companion object {
        private const val DIFFERENT_COLORS_NUMBER = 3
        private const val VIEW_IS_NULL = "DoorbellRecordViewHolder.onClick view is null"
    }
}