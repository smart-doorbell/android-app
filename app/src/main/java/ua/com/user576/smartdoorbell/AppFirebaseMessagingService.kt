package ua.com.user576.smartdoorbell

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class AppFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Log.i(NEW_TOKEN_TAG, token)
        // when some server will be created I will send token there
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.i(LOG_TAG, remoteMessage.notification?.imageUrl.toString())
        Log.i(LOG_TAG, remoteMessage.notification?.clickAction.toString())
    }

    companion object {

        private const val NEW_TOKEN_TAG = "NEW_TOKEN_TAG"
        private const val FAIL_MSG = "Fetching FCM registration token failed"

        internal fun logToken() {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(LOG_TAG, FAIL_MSG, task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token : String? = task.result

                Log.d(LOG_TAG, token.toString())
            })
        }
    }
}