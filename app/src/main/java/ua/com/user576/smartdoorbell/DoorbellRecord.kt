package ua.com.user576.smartdoorbell

private const val EMPTY_STRING = ""

data class DoorbellRecord(
    var key : Int = 0,
    var timeInstant : Long = 0L,
    var imageUrl : String = EMPTY_STRING,
)