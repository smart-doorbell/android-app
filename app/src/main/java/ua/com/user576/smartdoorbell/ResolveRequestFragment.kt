package ua.com.user576.smartdoorbell

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import ua.com.user576.smartdoorbell.databinding.ResolveRequestScreenBinding

class ResolveRequestFragment : Fragment() {

    private var binding : ResolveRequestScreenBinding? = null

    private val listener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            val record : DoorbellRecord? = snapshot.getValue(DoorbellRecord::class.java)
            if (record == null) {
                Log.w(LOG_TAG, RECORD_IS_NULL_LOG)
            } else {
                val ref = FirebaseStorage.getInstance().getReferenceFromUrl(record.imageUrl)
                binding?.let {
                    GlideApp.with(this@ResolveRequestFragment)
                        .load(ref)
                        .into(it.photoImageView)
                }
            }
        }

        override fun onCancelled(error: DatabaseError) {
            Log.w(LOG_TAG, ON_CANCELLED_LOG, error.toException())
        }
    }

    private var index : Int = 0
    private lateinit var recordReference : DatabaseReference
    private lateinit var firebaseDb : FirebaseDB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val nonNullBinding = ResolveRequestScreenBinding.inflate(inflater, container, false)
        binding = nonNullBinding

        nonNullBinding.timeInstantLabel.text = arguments?.getString(TIME_INSTANT_READABLE_STRING_KEY)
        index = requireArguments().getInt(DOORBELL_RECORD_KEY)
        firebaseDb = FirebaseDB()
        recordReference = firebaseDb.getRecordAt(index)
        recordReference.addValueEventListener(listener)

        nonNullBinding.approveBtn.setOnClickListener {
            approve()
        }
        nonNullBinding.denyBtn.setOnClickListener {
            deny()
        }

        return nonNullBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        recordReference.removeEventListener(listener)
        binding = null
    }

    private fun closeFragment() {
        recordReference.removeValue()
        activity?.let {
            if (it is MainActivity) {
                it.toRequestsList()
            }
        }
    }

    private fun approve() {
        firebaseDb.setLed(GREEN_LED)
        closeFragment()
    }

    private fun deny() {
        firebaseDb.setLed(RED_LED)
        closeFragment()
    }

    companion object {
        internal const val DOORBELL_RECORD_KEY = "doorbell_record_key"
        internal const val TIME_INSTANT_READABLE_STRING_KEY = "time_instant_readable_string_key"
        private const val GREEN_LED = "green"
        private const val RED_LED = "red"
        private const val RECORD_IS_NULL_LOG = "ValueEventListener.onDataChange: record is null"
        private const val ON_CANCELLED_LOG = "onCancelled@ResolveRequestFragment"
    }
}