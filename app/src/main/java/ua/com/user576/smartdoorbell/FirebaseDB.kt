package ua.com.user576.smartdoorbell

import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class FirebaseDB {

    init {
        AppFirebaseMessagingService.logToken()
    }

    val photosReference : DatabaseReference = FirebaseDatabase.getInstance(DB_URL).getReference("photos")

    val photosOptions = FirebaseRecyclerOptions.Builder<DoorbellRecord>()
        .setQuery(photosReference, DoorbellRecord::class.java)
        .build()

    fun getRecordAt(position : Int) : DatabaseReference = photosReference.child(position.toString())

    fun setLed(state : String) {
        FirebaseDatabase.getInstance(DB_URL).reference.child(LED).setValue(state)
    }

    companion object {
        private const val DB_URL = "https://smart-doorbell-576-default-rtdb.europe-west1.firebasedatabase.app"
        private const val LED = "led"
    }
}