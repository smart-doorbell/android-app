package ua.com.user576.smartdoorbell

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import ua.com.user576.smartdoorbell.databinding.RequestsListBinding

class RequestsListFragment : Fragment() {

    private var binding : RequestsListBinding? = null
    private lateinit var photosAdapter: PhotosAdapter

    private val listener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            updateLabels(snapshot.exists())
        }

        override fun onCancelled(error: DatabaseError) {
            Log.w(LOG_TAG, ON_CANCELLED_LOG, error.toException())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val nonNullBinding = RequestsListBinding.inflate(inflater, container, false)
        binding = nonNullBinding

        val database = FirebaseDB()
        photosAdapter = PhotosAdapter(requireContext(), database.photosOptions)
        photosAdapter.startListening()
        database.photosReference.addValueEventListener(listener)

        with(nonNullBinding) {
            noRequestsLabel.visibility = View.GONE
            recyclerDescription.visibility = View.GONE
            photosRecycleView.adapter = photosAdapter
        }

        return nonNullBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        photosAdapter.stopListening()
        binding = null
    }

    private fun updateLabels(areRequests : Boolean) {
        super.onStart()
        binding?.let {
            if (!areRequests) {
                it.noRequestsLabel.visibility = View.VISIBLE
                it.recyclerDescription.visibility = View.GONE
            } else {
                it.noRequestsLabel.visibility = View.GONE
                it.recyclerDescription.visibility = View.VISIBLE
            }
            it.progressBar.visibility = View.GONE
        }
    }

    companion object {
        private const val ON_CANCELLED_LOG = "onCancelled@RequstsListFragment"
    }
}