# Application has:

1) Recycler view with three view types;
2) Glide for image loading;
3) Firebase Realtime Database;
4) Firebase Authentication;
5) Firebase Cloud Storage;
6) Firebase notifications receiver.

# Описание приложения

Приложение SmartDoorbell было написано для смартфона в компьютерной системе SmartDoorbell. 

Компьютерная система SmartDoorbell состоит из трех компонентов: 
1. микроконтроллера ESP32CAM;
2. смартфона, на котором установлено приложение SmartDoorbell;
3. Firebase Realtime Database.

Подробнее об этой системе можно найти [тут](https://docs.google.com/document/d/15ysG-j5VHClpbRy0e3qQ37VGL5CRzIta/edit?usp=sharing&ouid=114011908728065365090&rtpof=true&sd=true)

Приложение SmartDoorbell отображает список активных запросов на доступ к дому и позволяет дать или запретить доступ.

Для того, чтобы проверить работоспособность приложения не обязательно иметь микроконтроллер ESP32CAM - можно воспользоваться эмулятором действий микроконтроллера, например, [этим](https://gitlab.com/smart-doorbell/esp32_emulator), как это было сделано в [этом видео](https://youtu.be/g9SKtR19S_4)

В [этом](https://gitlab.com/smart-doorbell/android-app/-/commit/dad9b125a7a339fc59ba8bf3bf4830c20e9bf0bc) коммите была добавлена Firebase Athentication. Для того, чтобы зайти в приложение надо ввести:

Email: test_email@gmail.com

Password: 123456

# References

Способы взаимодействия микроконтроллера и смартфона без промежуточного звена:
1) [прямое сетевое соединение](https://gitlab.com/smart-doorbell/net-only) без промежуточной Firebase Realtime Database;
2) передача данных через беспроводную [сеть Bluetooth](https://gitlab.com/smart-doorbell/bluetooth-app).
